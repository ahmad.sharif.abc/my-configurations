# zsh
sudo dnf zsh
sudo dnf oh

# ================  ZSH  ================

# installing powerline fonts:
git clone https://github.com/powerline/fonts.git --depth=1
# install
cd fonts
./install.sh
# clean-up a bit
cd ..
rm -rf fonts

# Install Awesome Terminal Fonts
git clone https://github.com/gabrielelana/awesome-terminal-fonts.git
cd awesome-terminal-fonts
cd build/
echo "\t===========\tManually install fonts \t==========="
mkdir ~/.fonts
cp build/*.sh ~/.fonts 
echo 'source ~/.fonts/*.sh' >> ~/.zshrc

# installing oh-my-zsh
# read this: https://virgool.io/@mimalef70/چگونه-یک-terminal-حرفه-ای-و-زیبا-در-macos-و-یا-linux-داشته-باشیم-cpfbh94swx2p#
sudo dnf zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# add zsh to allwoed shells
echo "\t===========\tadd zsh to /etc/shells manually \t==========="
echo "/usr/local/bin/zsh" >>/etc/shells

# ========= install Powerlevel9k zsh theme
git clone https://github.com/bhilburn/powerlevel9k.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel9k

# ========= set zsh as defualt shell
sudo usermode $USER -s $(which zsh) 
cp ~/.zshrc ~/.zshrc.back
cp ./config.zsh ~/.zshrc
source ~/.zshrc



# powerline
# awsomefont
# plugins

# openconnect
# set vpn config
sudo dnf install openconnect
sudo cp ./vpn /bin

# ssh key:
# github and gitlab and scarfbank_srv

# font for telegram

#mount data partition as /media #fstab file
