#! /bin/bash

# Disable Gatekeeper
# so any app can be installed
sudo spctl --master-disable

# make a link for UserData at home directory
ln -s /Volumes/UserData/ /Users/mohammadamir/

# # install jetbrains license server
# sudo /dvt-jb_licsrv.darwin.amd64 -mode install

# # generate SSH Key Pair and copy to VPS
# ssh-keygen 
# ssh-copy-id -p1414 root@jungle-im.ir

# # Copy keys from iCloud instead of this (iCloud/ssh) and set permissions
cp -r /Users/mohammad/Library/Mobile\ Documents/com~apple~CloudDocs/Backup/ssh ~/.ssh
chmod 400 .ssh/id_rsa

# install Homebrew
# from www.howtogeek.com/211541/
xcode-select --install
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew doctor

# Bash Completion added in ~/.bash_profile
brew install wget bash-completion

# Install Last version of Python 3
# brew install python3
# install Python 3.6.5
# From https://stackoverflow.com/a/51125014/4731038
brew install https://raw.githubusercontent.com/Homebrew/homebrew-core/f2a764ef944b1080be64bd88dca9a1d80130c558/Formula/python.rb

# install virtualenv for python 3
sudo pip3 install virtualenv

# install HoRNDIS(the USB tethering driver for Mac OS X)
# https://discussions.apple.com/thread/7693943
brew cask install horndis
# manually allow extention to work in "System Preferences > Security & Privacy > General" 
# ore detail at https://developer.apple.com/library/content/technotes/tn2459/_index.html
# sudo kextload /Library/Extensions/HoRNDIS.kext

# Install pip for python 2.7
sudo easy_install pip

# # make a command line shortcut for sublime
# sudo ln -s /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl /usr/local/bin/subl

# # Apache configuration
# mkdir Sites
# sudo mkdir /etc/apache2/my_confs
# sudo cp ~/Library/Mobile\ Documents/com~apple~CloudDocs/Backup/Apache /etc/apache2/httpd.conf
# sudo cp ~/Library/Mobile\ Documents/com~apple~CloudDocs/Backup/Apache /etc/apache2/my_confs/apps.conf

# copy back vimrc and bash_profile
cp ~/Library/Mobile\ Documents/com~apple~CloudDocs/Backup/Scripts/vimrc ~/.vimrc 
cp ~/Library/Mobile\ Documents/com~apple~CloudDocs/Backup/Scripts/bash_profile ~/.bash_profile 

# # set mysql PATH for current bash
# export PATH=/usr/local/mysql/bin:$PATH
# sudo mkdir /var/mysql
# sudo ln -s /tmp/mysql.sock /var/mysql/mysql.sock
# mysql_secure_installation

# Install ruby version manager for updating ruby
curl -L https://get.rvm.io | bash -s stable --auto-dotfiles --autolibs=enable --

# # install SASS for css
# sudo gem install -n /usr/local/bin sass
brew install sass/sass/sass

# Install node and coffescript
brew install node
# sudo curl -L https://npmjs.org/install.sh | sh
npm install -g coffee-script



# installing iterm2
# brew cask install iterm2


# ================  ZSH  ================

# installing powerline fonts:
git clone https://github.com/powerline/fonts.git --depth=1
# install
cd fonts
./install.sh
# clean-up a bit
cd ..
rm -rf fonts

# Install Awesome Terminal Fonts
# https://github.com/gabrielelana/awesome-terminal-fonts/wiki/OS-X
git clone https://github.com/gabrielelana/awesome-terminal-fonts.git
cd awesome-terminal-fonts
open build/
echo "\t===========\tManually install fonts \t==========="
mkdir ~/.fonts
cp build/*.sh ~/.fonts 
echo 'source ~/.fonts/*.sh' >> ~/.zshrc

# # install nerd-fonts
# brew tap caskroom/fonts
# brew cask install font-hack-nerd-font

# installing oh-my-zsh
# read this: https://virgool.io/@mimalef70/چگونه-یک-terminal-حرفه-ای-و-زیبا-در-macos-و-یا-linux-داشته-باشیم-cpfbh94swx2p#
brew install zsh zsh-completions zsh-syntax-highlighting
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
source ~/.zshrc
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# add zsh to allwoed shells
echo "\t===========\tadd zsh to /etc/shells manually \t==========="
# echo "/usr/local/bin/zsh" >>/etc/shells

# ========= install Powerlevel9k zsh theme
git clone https://github.com/bhilburn/powerlevel9k.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel9k

# ========= install alien zsh theme
# git clone https://github.com/eendroroy/alien.git ~/.oh-my-zsh/themes/alien
# cd ~/.oh-my-zsh/themes/alien
# git submodule update --init --recursive
# ln -s ~/.oh-my-zsh/themes/alien/alien.zsh ~/.oh-my-zsh/themes/alien.zsh-theme

# set zsh as default
chsh -s /usr/local/bin/zsh
